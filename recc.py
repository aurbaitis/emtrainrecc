import mysql.connector
import cmd
import string
from math import log
from porterstemmer import Stemmer

#input your credentials
cnx = mysql.connector.connect(user='', password='',
                              host='emtrain.cwvj79mlxv2f.us-east-1.rds.amazonaws.com',
                              database='emtrain_core')
cursor = cnx.cursor()

query = ("SELECT question, id FROM question_answers;")

cursor.execute(query)

question_dict = {} 
for (question, id) in cursor:
    question_dict[id] = question

cursor.close()
cnx.close()

stopwords = open(r'./stop.txt', 'r').read().splitlines()

stemmer = Stemmer()

def getwords(sentence):
    w = sentence.split()

    #remove all things that are 1 or 2 characters long (punctuation)
    w = [x for x in w if len(x)>2]

    #get rid of all the stop words
    w = [x for x in w if not x in stopwords]

    #stem each word so that e.g. happiness and happy are treated as the same
    w = [stemmer(x) for x in w]

    #get rid of dupes
    w = list(set(w))

    return w

#compute frequency of every word in the set. We want common words to count for less in the later analysis
freq = {}

for key in question_dict:
    words = getwords(question_dict[key])
    for word in words:
        freq[word] = freq.get(word, 0) + 1

n = len(freq)

#this is the question you want to find the recommendations for
question_id = raw_input("What id do you want to find related questions for? ")
target_question = question_dict[int(question_id)]
trainwords = getwords(target_question)
number = raw_input("How many questions to retrieve? ")
print '-----'

#the distances will be stored in this array as tuples of (score, index)
results = []

for key in question_dict:
    testwords = getwords(question_dict[key])
    #find all common words between the 2 sentences
    common_words = [x for x in trainwords if x in testwords]

    #compute similarity between the 2 sentences
    score = 0.0
    for word in common_words:
        #we take the inverse of the frequency based on the assumption that entries that share less common words are more related. We apply the log so that very uncommon words don't completely overshadow the effect on the score
        score += log(n/freq[word])

    results.append((score, key))

results.sort(reverse=True)

top = [x[1] for x in results[:int(number)+1]]
for i in top:
    print '-----'
    print 'id: {} q: {}'.format(i, question_dict[i])
